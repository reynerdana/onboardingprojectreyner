//
//  ContentView.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 06/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import SwiftUI
import UIKit
import AsyncDisplayKit
import RxSwift
import RxCocoa
import RxCocoa_Texture

final class ContentViewController: ASViewController<ASDisplayNode>, ASTableDataSource, ASTableDelegate {
	var isCommented = [Bool]()
	var postDataArray = [String]()
	@IBOutlet private var tableView: UITableView!
	private let disposeBag = DisposeBag()
	let coordinator = Coordinator()
	var createPostAction : PublishSubject<Void>?
	var viewModel: PostStoryViewModel?
    
    struct State {
      var itemCount: Int
      var fetchingMore: Bool
      static let empty = State(itemCount: 20, fetchingMore: false)
    }
    
    enum Action {
      case beginBatchFetch
      case endBatchFetch(resultCount: Int)
    }
    
    fileprivate(set) var state: State = .empty
    var tableNode: ASTableNode {
      return node as! ASTableNode
    }
    
    init() {
        super.init(node: ASTableNode())
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.backgroundColor = UIColor.white
		print("test again")
		isCommented = [true, false, true]
		postDataArray = ["Budi", "Doni", "Andi"]
		setupRightBarButtonItem()
        self.title = "test"
		self.createPostAction = PublishSubject()
    }
	
	func setupRightBarButtonItem(){
		let logoutBarButtonItem = UIBarButtonItem(title: "Post", style: .done, target: self, action: #selector(createPost))
		self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
	}
	
	@objc func createPost(){
		print("test like")
		self.viewModel?.createPostAction.onNext(())
	}
	
	
	
	override func viewDidLoad() {
		_ = Observable.from([1,2,3,4,5])
	}
    
    required init?(coder aDecoder: NSCoder) {
      fatalError("storyboards are incompatible with truth and beauty")
    }
    
    // MARK: ASTableNode data source and delegate.
//    func tableNode(_ tableNode: ASTableNode, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
//        <#code#>
//    }
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
           // let nodeView = ASDisplayNode()
			let node = ContentCellNode()
			node.postData = self.postDataArray
			node.profileName = self.postDataArray[indexPath.row]
			node.isCommented = self.isCommented[indexPath.row]
          return node
        }
    }
	
	func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
		print("test")
		
	}
    
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
//        var count = state.itemCount
//        if state.fetchingMore {
//          count += 1
//        }
//        return count
		return postDataArray.count
    }
	
	
    
//    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
//      /// This call will come in on a background thread. Switch to main
//      /// to add our spinner, then fire off our fetch.
//      DispatchQueue.main.async {
//        let oldState = self.state
//        self.state = ContentView.handleAction(.beginBatchFetch, fromState: oldState)
//        self.renderDiff(oldState)
//      }
//
//      ContentView.fetchDataWithCompletion { resultCount in
//        let action = Action.endBatchFetch(resultCount: resultCount)
//        let oldState = self.state
//        self.state = ContentView.handleAction(action, fromState: oldState)
//        self.renderDiff(oldState)
//        context.completeBatchFetching(true)
//      }
//    }
//
//    fileprivate func renderDiff(_ oldState: State) {
//
//      self.tableNode.performBatchUpdates({
//
//        // Add or remove items
//        let rowCountChange = state.itemCount - oldState.itemCount
//        if rowCountChange > 0 {
//          let indexPaths = (oldState.itemCount..<state.itemCount).map { index in
//            IndexPath(row: index, section: 0)
//          }
//          tableNode.insertRows(at: indexPaths, with: .none)
//        } else if rowCountChange < 0 {
//          assertionFailure("Deleting rows is not implemented. YAGNI.")
//        }
//
//        // Add or remove spinner.
//        if state.fetchingMore != oldState.fetchingMore {
//          if state.fetchingMore {
//            // Add spinner.
//            let spinnerIndexPath = IndexPath(row: state.itemCount, section: 0)
//            tableNode.insertRows(at: [ spinnerIndexPath ], with: .none)
//          } else {
//            // Remove spinner.
//            let spinnerIndexPath = IndexPath(row: oldState.itemCount, section: 0)
//            tableNode.deleteRows(at: [ spinnerIndexPath ], with: .none)
//          }
//        }
//      }, completion:nil)
//    }
//
//
//    fileprivate static func fetchDataWithCompletion(_ completion: @escaping (Int) -> Void) {
//      let time = DispatchTime.now() + Double(Int64(TimeInterval(NSEC_PER_SEC) * 1.0)) / Double(NSEC_PER_SEC)
//      DispatchQueue.main.asyncAfter(deadline: time) {
//        let resultCount = Int(arc4random_uniform(20))
//        completion(resultCount)
//      }
//    }
//
//    fileprivate static func handleAction(_ action: Action, fromState state: State) -> State {
//      var state = state
//      switch action {
//      case .beginBatchFetch:
//        state.fetchingMore = true
//      case let .endBatchFetch(resultCount):
//        state.itemCount += resultCount
//        state.fetchingMore = false
//      }
//      return state
//    }
}

//MARK: - Rx Setup
private extension ContentViewController {
	
	func setupCellTapHandling() {
		
		
//		tableNode
//			.rx
//			.modelSelected(People.self) //1
//			.subscribe(onNext: { [unowned self] chocolate in // 2
////				let newValue =  ShoppingCart.sharedCart.chocolates.value + [chocolate]
////				ShoppingCart.sharedCart.chocolates.accept(newValue) //3
////
//				if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
//					self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
//				} //4
//			})
//			.disposed(by: disposeBag) //5
	}
}

//struct ContentView: View {
//    var body: some View {
//        Text("Hello, World!")
//    }
//}
//
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
