//
//  TopHeaderViewNode.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 10/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import AsyncDisplayKit

class TopHeaderViewNode: ASDisplayNode {
	let infoTextNode 		= ASTextNode()
	let bottomSeparator = ASImageNode()
	var isCommented		= Bool()
	
	override init() {
        super.init()
		automaticallyManagesSubnodes = true
		let attrs = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 12.0)]
		let boldAttribute = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 14.0)]
		let tempString = "Reyner commented on this"
		let string = NSMutableAttributedString(string: tempString, attributes: attrs as [NSAttributedString.Key : Any])
		let nsString = NSString(string: tempString)
		let range = nsString.range(of: "Reyner")
		if range.length > 0 { string.setAttributes(boldAttribute, range: range) }
		infoTextNode.attributedText = string
		infoTextNode.style.alignSelf = .start
		bottomSeparator.image = UIImage.as_resizableRoundedImage(withCornerRadius: 1.0, cornerColor: .black, fill: .black)
		self.isCommented = true
    }
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		let photoDimension: CGFloat = constrainedSize.max.width
		bottomSeparator.style.flexGrow = 1.0
		bottomSeparator.style.preferredSize = CGSize.init(width: photoDimension, height: 1)
		var verticalStack = ASStackLayoutSpec()
		if(self.isCommented){
			verticalStack = ASStackLayoutSpec(direction: .vertical,
			spacing: 20,
			justifyContent: .center,
			alignItems: .start,
			children:[ infoTextNode, bottomSeparator])
		}else{
			verticalStack = ASStackLayoutSpec(direction: .vertical,
			spacing: 20,
			justifyContent: .center,
			alignItems: .start,
			children:[ infoTextNode])
		}
		
			
		
		let insetBox = ASInsetLayoutSpec(
			insets:	UIEdgeInsets(top: 10,left: 16,bottom: 10,right: 0),
			child: verticalStack
		)
		
		return insetBox
	}

}
