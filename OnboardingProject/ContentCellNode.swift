//
//  ContentCellNode.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 06/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class ContentCellNode: ASCellNode {
    let spinner = SpinnerNode()
    let text = ASTextNode()
    let imageNode = ASImageNode()
	let viewCell = LayoutViewNode()
	let topView  = TopHeaderViewNode()
	var arrayData = [String]()
	var postData = [String]()
	var profileName = String()
	var isCommented = Bool()
    
    override init() {
        super.init()
		arrayData = ["images", "King Hartono", "Managing Director at PT Dana Indonesia"]
		imageNode.image = UIImage(named: arrayData[0])
        imageNode.contentMode = .scaleAspectFill
        viewCell.userNamesString = "abc"
        text.attributedText = NSAttributedString(
            string: "Loading…",
            attributes: convertToOptionalNSAttributedStringKeyDictionary([
                convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont.systemFont(ofSize: 12),
                convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.lightGray,
                convertFromNSAttributedStringKey(NSAttributedString.Key.kern): -0.3
            ]))
		
		automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		if(isCommented){
			return ASStackLayoutSpec(
			direction: .vertical,
			spacing: 16,
			justifyContent: .center,
			alignItems: .center,
			children: [topView,viewCell])
		}else{
			return ASStackLayoutSpec(
			direction: .vertical,
			spacing: 16,
			justifyContent: .center,
			alignItems: .center,
			children: [viewCell])
		}
		
	}
}

final class SpinnerNode: ASDisplayNode {
  var activityIndicatorView: UIActivityIndicatorView {
    return view as! UIActivityIndicatorView
  }

  override init() {
    super.init()
    setViewBlock {
        UIActivityIndicatorView(style: .gray)
    }
    
    // Set spinner node to default size of the activitiy indicator view
//    self.style.preferredSize = CGSize(width: 20.0, height: 20.0)
  }

  override func didLoad() {
    super.didLoad()
    
    activityIndicatorView.startAnimating()
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}
