//
//  PostStoryViewModel.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 14/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PostStoryViewModel: NSObject {
	//var posts: BehaviorRelay<[Post]?> = BehaviorRelay(value: [Post]())
    var createPostAction: PublishSubject<Void>
	
	override init(){
		self.createPostAction = PublishSubject()
	}
}


