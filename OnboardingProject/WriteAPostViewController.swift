//
//  DetailContentView.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 11/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class WriteAPostViewController: ASViewController<ASDisplayNode> {
	private var contentNode: ASDisplayNode
	private var viewNode: WriteAPostViewNode
	
	init() {
		self.contentNode = ASDisplayNode()
		self.viewNode = WriteAPostViewNode()
		super.init(node: viewNode)
        self.title = "Detail"
    }
	
	required init?(coder aDecoder: NSCoder) {
      fatalError("storyboards are incompatible with truth and beauty")
    }
	
	
}

