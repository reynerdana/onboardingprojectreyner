//
//  People.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 12/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import Foundation

struct People : Equatable, Hashable{
	let profilePhoto: String
	let profileName: String
	let jobPosition: String
  
  // An array of chocolate from europe
  static let ofDana: [People] = {
	let Andi = People(profilePhoto: "images.jpeg", profileName: "Andi", jobPosition: "Manager")
	let Budi = People(profilePhoto: "images.jpeg", profileName: "Budi", jobPosition: "Supervisor")
	let Doni = People(profilePhoto: "images.jpeg", profileName: "Doni", jobPosition: "Director")
    
    
    return [Andi,Budi,Doni]
  }()
}


