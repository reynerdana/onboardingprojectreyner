

import Foundation
import AsyncDisplayKit


final class MainCoordinator {
    let coordinator = Coordinator()
    
    private var navigationController: ASNavigationController? {
        didSet {
            coordinator.navigationController = navigationController
        }
    }
    
    func start() {
        coordinator.start()
    }
}
