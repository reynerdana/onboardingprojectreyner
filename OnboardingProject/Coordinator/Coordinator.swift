//
//  Coordinator.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 13/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RxSwift

class Coordinator {
	var navigationController: ASNavigationController?
	var window = UIWindow(frame: UIScreen.main.bounds)
	let disposeBag 			= DisposeBag()
	
	init() {
		
	}
	
	func start() {
		let postStoryViewModel = PostStoryViewModel()
		let contentVC = ContentViewController()
		contentVC.viewModel = postStoryViewModel
		
		self.navigationController = ASNavigationController(rootViewController: contentVC)
		
		postStoryViewModel.createPostAction
			.bind(onNext: {[weak self] _ in
				self?.pushToDetailFromMain()
			})
			.disposed(by: disposeBag)
	}
	
	// MARK: - Private method

	func pushToDetailFromMain() {
		print("testing 2")
		let detailVC = WriteAPostViewController()
		self.navigationController?.pushViewController(detailVC, animated: true)
	}
	
//	func pushDetailViewController(viewModel: DetailTimelineViewModel?) {
//        let detailTimelineViewController = DetailTimelineViewController(viewModel: viewModel)
//        self.navigationController?.pushViewController(detailTimelineViewController, animated: true)
//    }
}



