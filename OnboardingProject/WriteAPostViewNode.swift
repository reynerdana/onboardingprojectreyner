//
//  DetailViewNode.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 11/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class WriteAPostViewNode: ASDisplayNode {
	let profileImageNode = ASImageNode()
	let postTypeButtonNode = ASButtonNode()
	let	postTextFieldNode = ASEditableTextNode()
	let blankViewNode = ASDisplayNode()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		setupView()
	}
	
	func setupView(){
		let width = ASDimensionMake(.auto, 0)
		let height = ASDimensionMake("20%")
		blankViewNode.style.preferredLayoutSize = ASLayoutSizeMake(width, height)
		profileImageNode.image = UIImage(named: "images.jpeg")
		profileImageNode.contentMode = .scaleAspectFill
		profileImageNode.cornerRadius = 20.0
		profileImageNode.style.preferredSize = CGSize(width: 32, height: 32)
		
		let attrs = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 12.0)]
		let string = NSAttributedString(string: "placeholder", attributes: attrs as [NSAttributedString.Key : Any])
		postTextFieldNode.attributedPlaceholderText = string
		
		let buttonLabelAttributes = [
			NSAttributedString.Key.foregroundColor : UIColor.darkGray,
			NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)
		]
		let buttonTitle = NSAttributedString(string: "Anyone",
										   attributes: buttonLabelAttributes)
		postTypeButtonNode.style.preferredSize = CGSize(width: 60, height: 32)
		postTypeButtonNode.contentMode = .scaleToFill
		postTypeButtonNode.setAttributedTitle(buttonTitle, for: .normal)
		postTypeButtonNode.imageAlignment = .beginning
		postTypeButtonNode.addTarget(self, action: #selector(buttonPressed), forControlEvents: .touchUpInside)
	}
	
	@objc func buttonPressed(){
		print("test like")
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		let outerStack = ASStackLayoutSpec.vertical()
		outerStack.spacing = 10
		outerStack.justifyContent = .start
		outerStack.alignItems = .center
		outerStack.children = [blankViewNode,profileImageNode,postTypeButtonNode,postTextFieldNode]
		return outerStack
	}
}
