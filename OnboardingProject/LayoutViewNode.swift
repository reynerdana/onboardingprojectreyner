//
//  LayoutViewNode.swift
//  OnboardingProject
//
//  Created by Reyner Subrata on 07/11/19.
//  Copyright © 2019 Reyner Subrata. All rights reserved.
//

import AsyncDisplayKit

class LayoutViewNode: ASDisplayNode {
	var userNamesString  = String()
	
    let userNameNode     = ASTextNode()
    let postLocationNode = ASTextNode()
    let postTimeNode     = ASTextNode()
	let positionDetailsNode = ASTextNode()
	
    let profileImageNode = ASNetworkImageNode()
	
	let	likeButtonNode 	 = ASButtonNode()
	let commentButtonNode = ASButtonNode()
	let shareButtonNode  = ASButtonNode()
	
	let imageTest		 = ASImageNode()
	let shareImageNode   = ASImageNode()
	let commentImageNode = ASImageNode()
	let likeImageNode 	 = ASImageNode()
	
	var arrayData		 = [String]()
	var arrayPosition 	 = [String]()
	let peopleData		 = People.ofDana
    
    override required init() {
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .white
		//arrayData = ["images.jpeg", "King Hartono", "Managing Director at PT Dana Indonesia"]
		let firstPeople = [peopleData[0].profilePhoto, peopleData[0].profileName, peopleData[0].jobPosition]
		arrayPosition = ["Marketing Manager \n", "Modern Channel \n", "Brand Marketing Executive \n", "Sales Specialist\n"]

		let attrs = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 12.0)]
		let string = NSAttributedString(string: firstPeople[1], attributes: attrs as [NSAttributedString.Key : Any])
		postTimeNode.attributedText = string
		
		let locationString = NSAttributedString(string: firstPeople[2], attributes: attrs as [NSAttributedString.Key : Any])
		postLocationNode.attributedText = locationString
		
		imageTest.image = UIImage(named: firstPeople[0])
		imageTest.contentMode = .scaleAspectFill
		imageTest.cornerRadius = 20.0
		let positionDetailsString = NSMutableAttributedString()
		
		for string in arrayPosition
		{
			let attrString = NSAttributedString(string: string)
			positionDetailsString.append(attrString)
		}
		
		positionDetailsNode.attributedText = positionDetailsString
		setupButton()
		
		//userNameNode.attributedText = NSAttributedString.attributedString(string: "Sunset Beach, San Fransisco, CA", fontSize: 20, color: .lightBlueColor())
		
		
    }
	
	func setupButton(){
		let buttonLabelAttributes = [
			NSAttributedString.Key.foregroundColor : UIColor.darkGray,
			NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)
		]
		let likeTitle = NSAttributedString(string: "Like",
										   attributes: buttonLabelAttributes)
		let shareTitle = NSAttributedString(string: "Share",
											attributes: buttonLabelAttributes)
		let commentTitle = NSAttributedString(string: "Comment",
											attributes: buttonLabelAttributes)
		
		likeButtonNode.style.preferredSize = CGSize(width: 60,
													height: 32)
		likeButtonNode.contentMode = .scaleToFill
		likeButtonNode.setAttributedTitle(likeTitle, for: .normal)
		likeButtonNode.imageAlignment = .end
		likeButtonNode.addTarget(self, action: #selector(buttonPressed), forControlEvents: .touchUpInside)
		
		likeImageNode.image = UIImage(named: "ic_like")
		likeImageNode.contentMode = .scaleAspectFit
		likeImageNode.style.preferredSize = CGSize(width: 32, height: 32)
		
		commentButtonNode.style.preferredSize = CGSize(width: 100,
													   height: 32)
		commentButtonNode.contentMode = .scaleAspectFill
		commentButtonNode.setAttributedTitle(commentTitle, for: .normal)
		
		commentImageNode.image = UIImage(named: "ic_comment")
		commentImageNode.contentMode = .scaleAspectFit
		commentImageNode.style.preferredSize = CGSize(width: 32, height: 32)
		
		shareButtonNode.style.preferredSize = CGSize(width: 60,
													 height: 32)
		shareButtonNode.contentMode = .scaleAspectFill
		shareButtonNode.setAttributedTitle(shareTitle, for: .normal)
		
		shareImageNode.image = UIImage(named: "ic_share")
		shareImageNode.contentMode = .scaleAspectFit
		shareImageNode.style.preferredSize = CGSize(width: 32, height: 32)
		
	}
	
	@objc func buttonPressed(){
		print("test like")
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		profileImageNode.style.preferredSize = CGSize(width: 100, height: 100)
		imageTest.style.preferredSize = CGSize(width: 100, height: 100)
		let detailStack = ASStackLayoutSpec.vertical()
		detailStack.children = [postTimeNode, postLocationNode]
		
		let profileBox = ASStackLayoutSpec.horizontal()
		profileBox.spacing = 10
		profileBox.justifyContent = .start
		profileBox.alignItems = .start
		profileBox.children = [imageTest, detailStack]
		
		let buttonStack = ASStackLayoutSpec(direction: .horizontal,
											spacing: 16,
											justifyContent: .start,
											alignItems: .start,
											children: [likeButtonNode])
		
		buttonStack.flexWrap = .wrap
		
		let likeBox = ASStackLayoutSpec(direction: .horizontal,
										spacing: 0,
										justifyContent: .start,
										alignItems: .start,
										children: [likeImageNode,likeButtonNode])
		
		let commentBox = ASStackLayoutSpec(direction: .horizontal,
										   spacing: 0,
										   justifyContent: .start,
										   alignItems: .start,
										   children: [commentImageNode,commentButtonNode])
		
		let shareBox = ASStackLayoutSpec(direction: .horizontal,
										 spacing: 0,
										 justifyContent: .start,
										 alignItems: .start,
										 children: [shareImageNode,shareButtonNode])
		
		let padding = ASStackLayoutSpec(direction: .horizontal,
										spacing: 0,
										justifyContent: .start,
										alignItems: .start,
										children: [likeBox,commentBox,shareBox])
		
		let outerBox = ASStackLayoutSpec.vertical()
		outerBox.justifyContent = .start
		outerBox.children = [profileBox,positionDetailsNode,padding]
		
		let insetBox = ASInsetLayoutSpec(
			insets:	UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10),
			child: outerBox
		)
		
		return insetBox
	}
    
    class func title() -> String {
      assertionFailure("All layout example nodes must provide a title!")
      return ""
    }

    class func descriptionTitle() -> String? {
      return nil
    }

}
